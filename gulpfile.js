var gulp = require("gulp"),
    watch = require("gulp-watch"),
    autoprefixer = require("autoprefixer"),
    postcss = require("gulp-postcss"),
    cssvars = require("postcss-simple-vars"),
    nested = require("postcss-nested"),
    cssImport = require("postcss-import");//when we're using gulp this module can import your modules from "node_modules".syntax is : @import packageName(which is in node_modules).
    browserSync = require("browser-sync").create();

//autoprefixer internally uses caniuse for detection.

gulp.task('default', function(){
   console.log("Hey! Gulp installed and running the default task!"); 
});

gulp.task("html", function(){
    console.log("Some cool stuff related to html is done here!");
});

gulp.task("css", function(){
    
    return gulp.src("./app/assets/styles/style.css")
        .pipe(postcss([cssImport,cssvars,nested,autoprefixer]))
        .pipe(gulp.dest("./app/css"));
});


gulp.task("watch", function(){
    
    //browserSyns(JSON object)
    browserSync.init({
        notify: false,
        server:{
            baseDir: "app"//path where index.html is saved
        }
    });
//    watch("./app/index.html", gulp.parallel("html","css")); for multiple tasks treated as threads.
    watch("./app/index.html", function(){
        browserSync.reload();
    });
    watch("./app/assets/styles/**/*.css",gulp.series("css", "cssInject")); 
});

gulp.task("cssInject", function(){
          return gulp.src("./app/css/style.css")
            .pipe(browserSync.stream());
})